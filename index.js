'use strict';

const R = require('ramda');

function sanitizeMarkdown(string = '') {
    string = string ? string.toString() : '';

    let r;
    let regexp = /(\[.+?]\(.+?\))|(\\)?([*_[`])/g;
    let alwaysEscape = ['['];
    let matches = {};
    let sequence = [];

    while ((r = regexp.exec(string)) !== null) {
        if (r[1]) continue;
        let literal = r[3];
        let escaped = false;
        let index = r.index;

        if (r[2] === '\\') {
            escaped = true;
            index++;
        }

        matches[literal] = matches[literal] || {literal, seq: []};

        let obj = {index, literal, escaped,
            literalSeqIndex: matches[literal].seq.length,
            generalSeqIndex: sequence.length
        };

        sequence.push(obj);

        matches[literal].seq[obj.literalSeqIndex] = obj;
    }

    string = string.split('');

    for (let i = 0; i < sequence.length; i++) {
        let current = sequence[i];
        if (current.escaped) continue;

        if (alwaysEscape.indexOf(current.literal) !== -1) {
            string.splice(current.index, 1, ['\\', current.literal]);
            continue;
        }

        let nextLiteral = matches[current.literal].seq[current.literalSeqIndex + 1];

        if (nextLiteral) {
            i = nextLiteral.generalSeqIndex;
            continue;
        }

        string.splice(current.index, 1, ['\\', current.literal]);
    }

    string = R.flatten(string).join('');

    return string;
}

module.exports = sanitizeMarkdown;