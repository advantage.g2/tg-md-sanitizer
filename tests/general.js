'use strict';

const should = require('should');
const sanitizeTGMD = require('../index');

describe(`eshared.general`, ()=>{
    it('sanitizeMarkdownMessage should escape md character not used in tag formation', ()=>{
        let message = `lol * _ [ kek * _ [ krab * _ [`;
        let expected = `lol * _ [ kek * _ [ krab * _ \\[`;
        sanitizeTGMD(message).should.be.equal(expected);
    });
    it('sanitizeMarkdownMessage should not change md link', ()=>{
        let message = `[lol](link://krab_*) lol *`;
        let expected = `[lol](link://krab_*) lol \\*`;
        sanitizeTGMD(message).should.be.equal(expected);
    });
    it('sanitizeMarkdownMessage should not include escaped characters', ()=>{
        let message = "[lol](link://krab_*) *lol* \\* * \\[ [`";
        let expected = "[lol](link://krab_*) *lol* \\* \\* \\[ \\[\\`";
        sanitizeTGMD(message).should.be.equal(expected);
    });

    it('sanitizeMarkdownMessage should not escape all links', ()=>{
        let message = `[Amnesia CBD](https://chel.errors-seeds-ru.net/image/catalog/Feminised/dinafem-amnesia-cbd.jpg)
[Подробнее](https://chel.errors-seeds-ru.net/index.php?route=product/product&product_id=1366)`;

        let expected = `[Amnesia CBD](https://chel.errors-seeds-ru.net/image/catalog/Feminised/dinafem-amnesia-cbd.jpg)
[Подробнее](https://chel.errors-seeds-ru.net/index.php?route=product/product&product_id=1366)`;
        sanitizeTGMD(message).should.be.equal(expected);
    });
    it('sanitizeMarkdownMessage should not escape inside md tags', ()=>{
        let message = "[lol](link://krab_*) *l_l*`";
        let expected = "[lol](link://krab_*) *l_l*\\`";
        sanitizeTGMD(message).should.be.equal(expected);
    });

    it('sanitizeMarkdownMessage should handle escapes only if they are not in the md tags', ()=>{
        let message = '*test\\* *test 2* \\*test 3_*';
        let expected = '*test\\* *test 2* \\*test 3\\_\\*';
        sanitizeTGMD(message).should.be.equal(expected);
    });
});

console.log(sanitizeTGMD('[lol](link://krab_*) *l_l*`   *test\\* *test 2* \\*test 3_*'))