# tg-md-sanitizer

Sanitizes incorrect markdown tags for telegram

## Usage

```js
const sanitizeTGMD = require('tg-md-sanitizer');

const str = '[lol](link://krab_*) *l_l*`   *test\* *test 2* \*test 3_*'

const sanitized = sanitizeTGMD(str);
```

**Input**

```
[lol](link://krab_*) *l_l*`   *test\* *test 2* \*test 3_*
```

**Output**

```
[lol](link://krab_*) *l_l*\`   *test\* *test 2* \*test 3\_\*
```

For more examples take a look on tests.